import os
from fastapi import FastAPI, Response, status
from prometheus_client import Gauge, generate_latest, CollectorRegistry, CONTENT_TYPE_LATEST
import requests

app = FastAPI()

# Prometheus metrics setup
registry = CollectorRegistry()
temperature_gauge = Gauge('weather_temperature_celsius', 'Current temperature in Celsius', registry=registry)
humidity_gauge = Gauge('weather_humidity_percent', 'Current humidity in percent', registry=registry)

# Read OpenWeatherMap API key from environment variable
API_KEY = os.getenv('OPENWEATHER_API_KEY')

@app.get('/metrics/{city}')
async def metrics(city: str):
    try:
        response = requests.get(f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={API_KEY}&units=metric')
        data = response.json()
        
        temperature = data['main']['temp']
        humidity = data['main']['humidity']
        
        temperature_gauge.set(temperature)
        humidity_gauge.set(humidity)
        
        return Response(content=generate_latest(registry), media_type=CONTENT_TYPE_LATEST)
    except Exception as e:
        return Response(content=str(e), status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

@app.get('/health')
async def health():
    return {"status": "ok"}

if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8000)