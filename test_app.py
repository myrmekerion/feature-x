import unittest
from fastapi.testclient import TestClient
from app import app

class BasicTests(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_health_check(self):
        response = self.client.get('/health')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'status': 'ok'})

if __name__ == "__main__":
    unittest.main()