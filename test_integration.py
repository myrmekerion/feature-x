import unittest
from fastapi.testclient import TestClient
from app import app
from unittest.mock import patch
import os

class IntegrationTests(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    @patch('app.requests.get')
    @patch.dict(os.environ, {'OPENWEATHER_API_KEY': 'fake_api_key'})
    def test_metrics(self, mock_get):
        mock_response = {
            'main': {
                'temp': 20.5,
                'humidity': 65
            }
        }
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = mock_response
        
        city = 'TestCity'
        response = self.client.get(f'/metrics/{city}')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'weather_temperature_celsius 20.5', response.content)
        self.assertIn(b'weather_humidity_percent 65', response.content)

if __name__ == "__main__":
    unittest.main()